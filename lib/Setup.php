<?php
/**
 * Created by PhpStorm.
 * User: gregbueno
 * Date: 11/11/14
 * Time: 5:15 PM
 */

namespace ObservantRecords\WordPress\Themes\Eponymous42020;

class Setup {

	public function __construct() {

	}

	public static function init() {

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'wp_enqueue_styles'), 21);

        add_action( 'pre_get_posts', function ( $query ) {
            if ( !is_admin() && $query->is_main_query() ) {
                if ( is_post_type_archive( 'album' ) ) {
                    set_query_var( 'posts_per_page', -1 );
                }
            }
        } );
	}

	public static function wp_enqueue_styles() {
		wp_dequeue_style( 'observantrecords2020-style' );

        wp_enqueue_style( 'eponymous42020-merriweather-font', '//fonts.googleapis.com/css?family=Merriweather+Sans:400,700,700italic,400italic' );
		wp_enqueue_style( 'eponymous42020-istok-font', '//fonts.googleapis.com/css?family=Istok+Web:400,700,700italic,400italic' );
		wp_enqueue_style( 'eponymous42020-style', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), null );
	}

}